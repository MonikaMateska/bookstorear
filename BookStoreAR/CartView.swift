import SwiftUI

struct CartView: View {
    let cartCount: Int
    let onResetCount: () -> Void

    var booksCountText: String {
        cartCount == 1 ? "книга 📕" : "книги 📚"
    }

    var body: some View {
        VStack(spacing: 12) {
            Text("Во вашата кошничка имате \(cartCount) \(booksCountText).")

            if cartCount > 0 {
                Text("Вкупно: \(cartCount * 650) ден")
                Button("Купиj", action: onResetCount)
                    .buttonStyle(.bordered)
            }
        }
    }
}
