import SwiftUI
import RealityKit

struct RootView: View {
    @State var cartCount = 0

    var body: some View {
        NavigationView {
            ARViewContainer(onCartTapped: onCartTapped)
                .edgesIgnoringSafeArea(.all)
                .navigationTitle("Преглед на книги")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        cartButton
                    }
                }
        }
    }

    var cartButton: some View {
        NavigationLink(destination: CartView(cartCount: cartCount,
                                             onResetCount: onResetCount)) {
            Image(systemName: "cart.fill")
                .resizable()
                .frame(width: 25, height: 25)
                .foregroundColor(.white)
                .overlay(alignment: .topTrailing) {
                    Text("\(cartCount)")
                        .font(.caption)
                        .foregroundColor(.white)
                        .padding(4)
                        .background(Color.blue)
                        .clipShape(Circle())
                        .offset(x: 14, y: -12)
                }
        }
        .padding(.horizontal)
    }

    private func onCartTapped(_ entity: Entity?) {
        withAnimation {
            cartCount += 1
        }
    }

    private func onResetCount() {
        withAnimation {
            cartCount = 0
        }
    }
}

struct ARViewContainer: UIViewRepresentable {
    let onCartTapped: (Entity?) -> Void
    
    func makeUIView(context: Context) -> ARView {
        let arView = ARView(frame: .zero)
        let infoAnchor = try! Experience.loadInfo()

        infoAnchor.actions.addToCartTapped.onAction = onCartTapped

        arView.scene.anchors.append(infoAnchor)
        
        return arView
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {}
    
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
#endif
